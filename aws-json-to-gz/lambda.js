let AWS = require('aws-sdk');
const s3 = new AWS.S3();
const pako = require('pako');

exports.handler = function (event, context, callback) {

    var test = { my: 'super', puper: [456, 567], awesome: 'pako' };

    var binaryString = pako.deflate(JSON.stringify(test), { to: 'string' });
    
    s3.putObject({
        "Body": binaryString,
        "Bucket": "json-to-gz",
        "Key": "gzfile"
    })
        .promise()
        .then(data => {
            console.log(data);           // successful response
            /*
            data = {
                ETag: "\\"6805f2cfc46c0f04559748bb039d69ae\\"", 
                VersionId: "pSKidl4pHBiNwukdbcPXAIs.sshFFOc0"
            }
            */
        })
        .catch(err => {
            console.log(err, err.stack); // an error occurred
        });

    callback(null, { "message": "Successfully executed" });
}